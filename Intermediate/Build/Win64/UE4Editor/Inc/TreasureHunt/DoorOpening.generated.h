// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TREASUREHUNT_DoorOpening_generated_h
#error "DoorOpening.generated.h already included, missing '#pragma once' in DoorOpening.h"
#endif
#define TREASUREHUNT_DoorOpening_generated_h

#define TreasureHunt_Source_TreasureHunt_DoorOpening_h_16_SPARSE_DATA
#define TreasureHunt_Source_TreasureHunt_DoorOpening_h_16_RPC_WRAPPERS
#define TreasureHunt_Source_TreasureHunt_DoorOpening_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define TreasureHunt_Source_TreasureHunt_DoorOpening_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDoorOpening(); \
	friend struct Z_Construct_UClass_UDoorOpening_Statics; \
public: \
	DECLARE_CLASS(UDoorOpening, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TreasureHunt"), NO_API) \
	DECLARE_SERIALIZER(UDoorOpening)


#define TreasureHunt_Source_TreasureHunt_DoorOpening_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUDoorOpening(); \
	friend struct Z_Construct_UClass_UDoorOpening_Statics; \
public: \
	DECLARE_CLASS(UDoorOpening, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TreasureHunt"), NO_API) \
	DECLARE_SERIALIZER(UDoorOpening)


#define TreasureHunt_Source_TreasureHunt_DoorOpening_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDoorOpening(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDoorOpening) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDoorOpening); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDoorOpening); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDoorOpening(UDoorOpening&&); \
	NO_API UDoorOpening(const UDoorOpening&); \
public:


#define TreasureHunt_Source_TreasureHunt_DoorOpening_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDoorOpening(UDoorOpening&&); \
	NO_API UDoorOpening(const UDoorOpening&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDoorOpening); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDoorOpening); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDoorOpening)


#define TreasureHunt_Source_TreasureHunt_DoorOpening_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DoorOpenSpeed() { return STRUCT_OFFSET(UDoorOpening, DoorOpenSpeed); } \
	FORCEINLINE static uint32 __PPO__TargetDelay() { return STRUCT_OFFSET(UDoorOpening, TargetDelay); } \
	FORCEINLINE static uint32 __PPO__DoorAudio() { return STRUCT_OFFSET(UDoorOpening, DoorAudio); } \
	FORCEINLINE static uint32 __PPO__PressurePlate1() { return STRUCT_OFFSET(UDoorOpening, PressurePlate1); } \
	FORCEINLINE static uint32 __PPO__PressurePlate2() { return STRUCT_OFFSET(UDoorOpening, PressurePlate2); } \
	FORCEINLINE static uint32 __PPO__PressurePlate3() { return STRUCT_OFFSET(UDoorOpening, PressurePlate3); } \
	FORCEINLINE static uint32 __PPO__PressurePlate4() { return STRUCT_OFFSET(UDoorOpening, PressurePlate4); } \
	FORCEINLINE static uint32 __PPO__GameWonTrigger() { return STRUCT_OFFSET(UDoorOpening, GameWonTrigger); } \
	FORCEINLINE static uint32 __PPO__Cube1() { return STRUCT_OFFSET(UDoorOpening, Cube1); } \
	FORCEINLINE static uint32 __PPO__Cube2() { return STRUCT_OFFSET(UDoorOpening, Cube2); } \
	FORCEINLINE static uint32 __PPO__Cube3() { return STRUCT_OFFSET(UDoorOpening, Cube3); } \
	FORCEINLINE static uint32 __PPO__Cube4() { return STRUCT_OFFSET(UDoorOpening, Cube4); }


#define TreasureHunt_Source_TreasureHunt_DoorOpening_h_13_PROLOG
#define TreasureHunt_Source_TreasureHunt_DoorOpening_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TreasureHunt_Source_TreasureHunt_DoorOpening_h_16_PRIVATE_PROPERTY_OFFSET \
	TreasureHunt_Source_TreasureHunt_DoorOpening_h_16_SPARSE_DATA \
	TreasureHunt_Source_TreasureHunt_DoorOpening_h_16_RPC_WRAPPERS \
	TreasureHunt_Source_TreasureHunt_DoorOpening_h_16_INCLASS \
	TreasureHunt_Source_TreasureHunt_DoorOpening_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TreasureHunt_Source_TreasureHunt_DoorOpening_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TreasureHunt_Source_TreasureHunt_DoorOpening_h_16_PRIVATE_PROPERTY_OFFSET \
	TreasureHunt_Source_TreasureHunt_DoorOpening_h_16_SPARSE_DATA \
	TreasureHunt_Source_TreasureHunt_DoorOpening_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	TreasureHunt_Source_TreasureHunt_DoorOpening_h_16_INCLASS_NO_PURE_DECLS \
	TreasureHunt_Source_TreasureHunt_DoorOpening_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TREASUREHUNT_API UClass* StaticClass<class UDoorOpening>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TreasureHunt_Source_TreasureHunt_DoorOpening_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
