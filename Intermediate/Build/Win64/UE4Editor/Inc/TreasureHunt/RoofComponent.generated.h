// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TREASUREHUNT_RoofComponent_generated_h
#error "RoofComponent.generated.h already included, missing '#pragma once' in RoofComponent.h"
#endif
#define TREASUREHUNT_RoofComponent_generated_h

#define TreasureHunt_Source_TreasureHunt_RoofComponent_h_18_SPARSE_DATA
#define TreasureHunt_Source_TreasureHunt_RoofComponent_h_18_RPC_WRAPPERS
#define TreasureHunt_Source_TreasureHunt_RoofComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define TreasureHunt_Source_TreasureHunt_RoofComponent_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURoofComponent(); \
	friend struct Z_Construct_UClass_URoofComponent_Statics; \
public: \
	DECLARE_CLASS(URoofComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TreasureHunt"), NO_API) \
	DECLARE_SERIALIZER(URoofComponent)


#define TreasureHunt_Source_TreasureHunt_RoofComponent_h_18_INCLASS \
private: \
	static void StaticRegisterNativesURoofComponent(); \
	friend struct Z_Construct_UClass_URoofComponent_Statics; \
public: \
	DECLARE_CLASS(URoofComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TreasureHunt"), NO_API) \
	DECLARE_SERIALIZER(URoofComponent)


#define TreasureHunt_Source_TreasureHunt_RoofComponent_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URoofComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URoofComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URoofComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URoofComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URoofComponent(URoofComponent&&); \
	NO_API URoofComponent(const URoofComponent&); \
public:


#define TreasureHunt_Source_TreasureHunt_RoofComponent_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URoofComponent(URoofComponent&&); \
	NO_API URoofComponent(const URoofComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URoofComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URoofComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URoofComponent)


#define TreasureHunt_Source_TreasureHunt_RoofComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CamShake() { return STRUCT_OFFSET(URoofComponent, CamShake); } \
	FORCEINLINE static uint32 __PPO__WidgetClass() { return STRUCT_OFFSET(URoofComponent, WidgetClass); } \
	FORCEINLINE static uint32 __PPO__GameOverWidget() { return STRUCT_OFFSET(URoofComponent, GameOverWidget); }


#define TreasureHunt_Source_TreasureHunt_RoofComponent_h_15_PROLOG
#define TreasureHunt_Source_TreasureHunt_RoofComponent_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TreasureHunt_Source_TreasureHunt_RoofComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	TreasureHunt_Source_TreasureHunt_RoofComponent_h_18_SPARSE_DATA \
	TreasureHunt_Source_TreasureHunt_RoofComponent_h_18_RPC_WRAPPERS \
	TreasureHunt_Source_TreasureHunt_RoofComponent_h_18_INCLASS \
	TreasureHunt_Source_TreasureHunt_RoofComponent_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TreasureHunt_Source_TreasureHunt_RoofComponent_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TreasureHunt_Source_TreasureHunt_RoofComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	TreasureHunt_Source_TreasureHunt_RoofComponent_h_18_SPARSE_DATA \
	TreasureHunt_Source_TreasureHunt_RoofComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	TreasureHunt_Source_TreasureHunt_RoofComponent_h_18_INCLASS_NO_PURE_DECLS \
	TreasureHunt_Source_TreasureHunt_RoofComponent_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TREASUREHUNT_API UClass* StaticClass<class URoofComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TreasureHunt_Source_TreasureHunt_RoofComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
