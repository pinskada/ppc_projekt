// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TreasureHunt/RoofComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRoofComponent() {}
// Cross Module References
	TREASUREHUNT_API UClass* Z_Construct_UClass_URoofComponent_NoRegister();
	TREASUREHUNT_API UClass* Z_Construct_UClass_URoofComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_TreasureHunt();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	TREASUREHUNT_API UClass* Z_Construct_UClass_UCameraShaker_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UUserWidget_NoRegister();
	TREASUREHUNT_API UClass* Z_Construct_UClass_UGameOverWidget_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UAudioComponent_NoRegister();
// End Cross Module References
	void URoofComponent::StaticRegisterNativesURoofComponent()
	{
	}
	UClass* Z_Construct_UClass_URoofComponent_NoRegister()
	{
		return URoofComponent::StaticClass();
	}
	struct Z_Construct_UClass_URoofComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CamShake_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_CamShake;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WidgetClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_WidgetClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GameOverWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GameOverWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GameOver_MetaData[];
#endif
		static void NewProp_GameOver_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_GameOver;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RoofLowerSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RoofLowerSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RoofAudio_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RoofAudio;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URoofComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_TreasureHunt,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URoofComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "RoofComponent.h" },
		{ "ModuleRelativePath", "RoofComponent.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URoofComponent_Statics::NewProp_CamShake_MetaData[] = {
		{ "Category", "RoofComponent" },
		{ "ModuleRelativePath", "RoofComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_URoofComponent_Statics::NewProp_CamShake = { "CamShake", nullptr, (EPropertyFlags)0x0024080000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URoofComponent, CamShake), Z_Construct_UClass_UCameraShaker_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_URoofComponent_Statics::NewProp_CamShake_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URoofComponent_Statics::NewProp_CamShake_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URoofComponent_Statics::NewProp_WidgetClass_MetaData[] = {
		{ "Category", "Class Types" },
		{ "ModuleRelativePath", "RoofComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_URoofComponent_Statics::NewProp_WidgetClass = { "WidgetClass", nullptr, (EPropertyFlags)0x0024080000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URoofComponent, WidgetClass), Z_Construct_UClass_UUserWidget_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_URoofComponent_Statics::NewProp_WidgetClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URoofComponent_Statics::NewProp_WidgetClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URoofComponent_Statics::NewProp_GameOverWidget_MetaData[] = {
		{ "Category", "Runtime" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "RoofComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URoofComponent_Statics::NewProp_GameOverWidget = { "GameOverWidget", nullptr, (EPropertyFlags)0x00200800000a0809, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URoofComponent, GameOverWidget), Z_Construct_UClass_UGameOverWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URoofComponent_Statics::NewProp_GameOverWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URoofComponent_Statics::NewProp_GameOverWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URoofComponent_Statics::NewProp_GameOver_MetaData[] = {
		{ "Category", "RoofComponent" },
		{ "ModuleRelativePath", "RoofComponent.h" },
	};
#endif
	void Z_Construct_UClass_URoofComponent_Statics::NewProp_GameOver_SetBit(void* Obj)
	{
		((URoofComponent*)Obj)->GameOver = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URoofComponent_Statics::NewProp_GameOver = { "GameOver", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URoofComponent), &Z_Construct_UClass_URoofComponent_Statics::NewProp_GameOver_SetBit, METADATA_PARAMS(Z_Construct_UClass_URoofComponent_Statics::NewProp_GameOver_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URoofComponent_Statics::NewProp_GameOver_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URoofComponent_Statics::NewProp_RoofLowerSpeed_MetaData[] = {
		{ "Category", "RoofComponent" },
		{ "ModuleRelativePath", "RoofComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_URoofComponent_Statics::NewProp_RoofLowerSpeed = { "RoofLowerSpeed", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URoofComponent, RoofLowerSpeed), METADATA_PARAMS(Z_Construct_UClass_URoofComponent_Statics::NewProp_RoofLowerSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URoofComponent_Statics::NewProp_RoofLowerSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URoofComponent_Statics::NewProp_RoofAudio_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "RoofComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URoofComponent_Statics::NewProp_RoofAudio = { "RoofAudio", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URoofComponent, RoofAudio), Z_Construct_UClass_UAudioComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URoofComponent_Statics::NewProp_RoofAudio_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URoofComponent_Statics::NewProp_RoofAudio_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URoofComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URoofComponent_Statics::NewProp_CamShake,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URoofComponent_Statics::NewProp_WidgetClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URoofComponent_Statics::NewProp_GameOverWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URoofComponent_Statics::NewProp_GameOver,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URoofComponent_Statics::NewProp_RoofLowerSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URoofComponent_Statics::NewProp_RoofAudio,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URoofComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URoofComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URoofComponent_Statics::ClassParams = {
		&URoofComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URoofComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URoofComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_URoofComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URoofComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URoofComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URoofComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URoofComponent, 2187557473);
	template<> TREASUREHUNT_API UClass* StaticClass<URoofComponent>()
	{
		return URoofComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URoofComponent(Z_Construct_UClass_URoofComponent, &URoofComponent::StaticClass, TEXT("/Script/TreasureHunt"), TEXT("URoofComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URoofComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
