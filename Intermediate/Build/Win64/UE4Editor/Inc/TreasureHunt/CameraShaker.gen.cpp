// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TreasureHunt/Public/CameraShaker.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCameraShaker() {}
// Cross Module References
	TREASUREHUNT_API UClass* Z_Construct_UClass_UCameraShaker_NoRegister();
	TREASUREHUNT_API UClass* Z_Construct_UClass_UCameraShaker();
	ENGINE_API UClass* Z_Construct_UClass_UMatineeCameraShake();
	UPackage* Z_Construct_UPackage__Script_TreasureHunt();
// End Cross Module References
	void UCameraShaker::StaticRegisterNativesUCameraShaker()
	{
	}
	UClass* Z_Construct_UClass_UCameraShaker_NoRegister()
	{
		return UCameraShaker::StaticClass();
	}
	struct Z_Construct_UClass_UCameraShaker_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCameraShaker_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMatineeCameraShake,
		(UObject* (*)())Z_Construct_UPackage__Script_TreasureHunt,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraShaker_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CameraShaker.h" },
		{ "ModuleRelativePath", "Public/CameraShaker.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCameraShaker_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCameraShaker>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCameraShaker_Statics::ClassParams = {
		&UCameraShaker::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCameraShaker_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCameraShaker_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCameraShaker()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCameraShaker_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCameraShaker, 2585374291);
	template<> TREASUREHUNT_API UClass* StaticClass<UCameraShaker>()
	{
		return UCameraShaker::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCameraShaker(Z_Construct_UClass_UCameraShaker, &UCameraShaker::StaticClass, TEXT("/Script/TreasureHunt"), TEXT("UCameraShaker"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCameraShaker);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
