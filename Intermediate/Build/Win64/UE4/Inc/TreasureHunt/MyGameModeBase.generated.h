// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TREASUREHUNT_MyGameModeBase_generated_h
#error "MyGameModeBase.generated.h already included, missing '#pragma once' in MyGameModeBase.h"
#endif
#define TREASUREHUNT_MyGameModeBase_generated_h

#define TreasureHunt_Source_TreasureHunt_MyGameModeBase_h_13_SPARSE_DATA
#define TreasureHunt_Source_TreasureHunt_MyGameModeBase_h_13_RPC_WRAPPERS
#define TreasureHunt_Source_TreasureHunt_MyGameModeBase_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define TreasureHunt_Source_TreasureHunt_MyGameModeBase_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyGameModeBase(); \
	friend struct Z_Construct_UClass_AMyGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AMyGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TreasureHunt"), NO_API) \
	DECLARE_SERIALIZER(AMyGameModeBase)


#define TreasureHunt_Source_TreasureHunt_MyGameModeBase_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAMyGameModeBase(); \
	friend struct Z_Construct_UClass_AMyGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AMyGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TreasureHunt"), NO_API) \
	DECLARE_SERIALIZER(AMyGameModeBase)


#define TreasureHunt_Source_TreasureHunt_MyGameModeBase_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyGameModeBase(AMyGameModeBase&&); \
	NO_API AMyGameModeBase(const AMyGameModeBase&); \
public:


#define TreasureHunt_Source_TreasureHunt_MyGameModeBase_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyGameModeBase(AMyGameModeBase&&); \
	NO_API AMyGameModeBase(const AMyGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyGameModeBase)


#define TreasureHunt_Source_TreasureHunt_MyGameModeBase_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__GameOverWidgetClass() { return STRUCT_OFFSET(AMyGameModeBase, GameOverWidgetClass); } \
	FORCEINLINE static uint32 __PPO__GameOverWidget() { return STRUCT_OFFSET(AMyGameModeBase, GameOverWidget); } \
	FORCEINLINE static uint32 __PPO__GameWonWidgetClass() { return STRUCT_OFFSET(AMyGameModeBase, GameWonWidgetClass); } \
	FORCEINLINE static uint32 __PPO__GameWonWidget() { return STRUCT_OFFSET(AMyGameModeBase, GameWonWidget); }


#define TreasureHunt_Source_TreasureHunt_MyGameModeBase_h_10_PROLOG
#define TreasureHunt_Source_TreasureHunt_MyGameModeBase_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TreasureHunt_Source_TreasureHunt_MyGameModeBase_h_13_PRIVATE_PROPERTY_OFFSET \
	TreasureHunt_Source_TreasureHunt_MyGameModeBase_h_13_SPARSE_DATA \
	TreasureHunt_Source_TreasureHunt_MyGameModeBase_h_13_RPC_WRAPPERS \
	TreasureHunt_Source_TreasureHunt_MyGameModeBase_h_13_INCLASS \
	TreasureHunt_Source_TreasureHunt_MyGameModeBase_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TreasureHunt_Source_TreasureHunt_MyGameModeBase_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TreasureHunt_Source_TreasureHunt_MyGameModeBase_h_13_PRIVATE_PROPERTY_OFFSET \
	TreasureHunt_Source_TreasureHunt_MyGameModeBase_h_13_SPARSE_DATA \
	TreasureHunt_Source_TreasureHunt_MyGameModeBase_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	TreasureHunt_Source_TreasureHunt_MyGameModeBase_h_13_INCLASS_NO_PURE_DECLS \
	TreasureHunt_Source_TreasureHunt_MyGameModeBase_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TREASUREHUNT_API UClass* StaticClass<class AMyGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TreasureHunt_Source_TreasureHunt_MyGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
