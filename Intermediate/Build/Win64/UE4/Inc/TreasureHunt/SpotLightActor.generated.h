// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TREASUREHUNT_SpotLightActor_generated_h
#error "SpotLightActor.generated.h already included, missing '#pragma once' in SpotLightActor.h"
#endif
#define TREASUREHUNT_SpotLightActor_generated_h

#define TreasureHunt_Source_TreasureHunt_Public_SpotLightActor_h_16_SPARSE_DATA
#define TreasureHunt_Source_TreasureHunt_Public_SpotLightActor_h_16_RPC_WRAPPERS
#define TreasureHunt_Source_TreasureHunt_Public_SpotLightActor_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define TreasureHunt_Source_TreasureHunt_Public_SpotLightActor_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpotLightActor(); \
	friend struct Z_Construct_UClass_ASpotLightActor_Statics; \
public: \
	DECLARE_CLASS(ASpotLightActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TreasureHunt"), NO_API) \
	DECLARE_SERIALIZER(ASpotLightActor)


#define TreasureHunt_Source_TreasureHunt_Public_SpotLightActor_h_16_INCLASS \
private: \
	static void StaticRegisterNativesASpotLightActor(); \
	friend struct Z_Construct_UClass_ASpotLightActor_Statics; \
public: \
	DECLARE_CLASS(ASpotLightActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TreasureHunt"), NO_API) \
	DECLARE_SERIALIZER(ASpotLightActor)


#define TreasureHunt_Source_TreasureHunt_Public_SpotLightActor_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpotLightActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpotLightActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpotLightActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpotLightActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpotLightActor(ASpotLightActor&&); \
	NO_API ASpotLightActor(const ASpotLightActor&); \
public:


#define TreasureHunt_Source_TreasureHunt_Public_SpotLightActor_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpotLightActor(ASpotLightActor&&); \
	NO_API ASpotLightActor(const ASpotLightActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpotLightActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpotLightActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpotLightActor)


#define TreasureHunt_Source_TreasureHunt_Public_SpotLightActor_h_16_PRIVATE_PROPERTY_OFFSET
#define TreasureHunt_Source_TreasureHunt_Public_SpotLightActor_h_13_PROLOG
#define TreasureHunt_Source_TreasureHunt_Public_SpotLightActor_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TreasureHunt_Source_TreasureHunt_Public_SpotLightActor_h_16_PRIVATE_PROPERTY_OFFSET \
	TreasureHunt_Source_TreasureHunt_Public_SpotLightActor_h_16_SPARSE_DATA \
	TreasureHunt_Source_TreasureHunt_Public_SpotLightActor_h_16_RPC_WRAPPERS \
	TreasureHunt_Source_TreasureHunt_Public_SpotLightActor_h_16_INCLASS \
	TreasureHunt_Source_TreasureHunt_Public_SpotLightActor_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TreasureHunt_Source_TreasureHunt_Public_SpotLightActor_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TreasureHunt_Source_TreasureHunt_Public_SpotLightActor_h_16_PRIVATE_PROPERTY_OFFSET \
	TreasureHunt_Source_TreasureHunt_Public_SpotLightActor_h_16_SPARSE_DATA \
	TreasureHunt_Source_TreasureHunt_Public_SpotLightActor_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	TreasureHunt_Source_TreasureHunt_Public_SpotLightActor_h_16_INCLASS_NO_PURE_DECLS \
	TreasureHunt_Source_TreasureHunt_Public_SpotLightActor_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TREASUREHUNT_API UClass* StaticClass<class ASpotLightActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TreasureHunt_Source_TreasureHunt_Public_SpotLightActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
