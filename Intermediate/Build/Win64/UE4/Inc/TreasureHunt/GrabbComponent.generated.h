// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TREASUREHUNT_GrabbComponent_generated_h
#error "GrabbComponent.generated.h already included, missing '#pragma once' in GrabbComponent.h"
#endif
#define TREASUREHUNT_GrabbComponent_generated_h

#define TreasureHunt_Source_TreasureHunt_GrabbComponent_h_16_SPARSE_DATA
#define TreasureHunt_Source_TreasureHunt_GrabbComponent_h_16_RPC_WRAPPERS
#define TreasureHunt_Source_TreasureHunt_GrabbComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define TreasureHunt_Source_TreasureHunt_GrabbComponent_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGrabbComponent(); \
	friend struct Z_Construct_UClass_UGrabbComponent_Statics; \
public: \
	DECLARE_CLASS(UGrabbComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TreasureHunt"), NO_API) \
	DECLARE_SERIALIZER(UGrabbComponent)


#define TreasureHunt_Source_TreasureHunt_GrabbComponent_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUGrabbComponent(); \
	friend struct Z_Construct_UClass_UGrabbComponent_Statics; \
public: \
	DECLARE_CLASS(UGrabbComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TreasureHunt"), NO_API) \
	DECLARE_SERIALIZER(UGrabbComponent)


#define TreasureHunt_Source_TreasureHunt_GrabbComponent_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGrabbComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGrabbComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGrabbComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGrabbComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGrabbComponent(UGrabbComponent&&); \
	NO_API UGrabbComponent(const UGrabbComponent&); \
public:


#define TreasureHunt_Source_TreasureHunt_GrabbComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGrabbComponent(UGrabbComponent&&); \
	NO_API UGrabbComponent(const UGrabbComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGrabbComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGrabbComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGrabbComponent)


#define TreasureHunt_Source_TreasureHunt_GrabbComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Reach() { return STRUCT_OFFSET(UGrabbComponent, Reach); }


#define TreasureHunt_Source_TreasureHunt_GrabbComponent_h_13_PROLOG
#define TreasureHunt_Source_TreasureHunt_GrabbComponent_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TreasureHunt_Source_TreasureHunt_GrabbComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	TreasureHunt_Source_TreasureHunt_GrabbComponent_h_16_SPARSE_DATA \
	TreasureHunt_Source_TreasureHunt_GrabbComponent_h_16_RPC_WRAPPERS \
	TreasureHunt_Source_TreasureHunt_GrabbComponent_h_16_INCLASS \
	TreasureHunt_Source_TreasureHunt_GrabbComponent_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TreasureHunt_Source_TreasureHunt_GrabbComponent_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TreasureHunt_Source_TreasureHunt_GrabbComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	TreasureHunt_Source_TreasureHunt_GrabbComponent_h_16_SPARSE_DATA \
	TreasureHunt_Source_TreasureHunt_GrabbComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	TreasureHunt_Source_TreasureHunt_GrabbComponent_h_16_INCLASS_NO_PURE_DECLS \
	TreasureHunt_Source_TreasureHunt_GrabbComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TREASUREHUNT_API UClass* StaticClass<class UGrabbComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TreasureHunt_Source_TreasureHunt_GrabbComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
