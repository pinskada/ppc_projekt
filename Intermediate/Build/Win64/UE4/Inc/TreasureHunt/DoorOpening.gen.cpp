// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TreasureHunt/DoorOpening.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDoorOpening() {}
// Cross Module References
	TREASUREHUNT_API UClass* Z_Construct_UClass_UDoorOpening_NoRegister();
	TREASUREHUNT_API UClass* Z_Construct_UClass_UDoorOpening();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_TreasureHunt();
	ENGINE_API UClass* Z_Construct_UClass_UAudioComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_ATriggerVolume_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	void UDoorOpening::StaticRegisterNativesUDoorOpening()
	{
	}
	UClass* Z_Construct_UClass_UDoorOpening_NoRegister()
	{
		return UDoorOpening::StaticClass();
	}
	struct Z_Construct_UClass_UDoorOpening_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DoorOpenSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DoorOpenSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetDelay_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TargetDelay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DoorAudio_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DoorAudio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PressurePlate1_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PressurePlate1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PressurePlate2_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PressurePlate2;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PressurePlate3_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PressurePlate3;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PressurePlate4_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PressurePlate4;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GameWonTrigger_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GameWonTrigger;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Cube1_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Cube1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Cube2_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Cube2;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Cube3_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Cube3;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Cube4_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Cube4;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDoorOpening_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_TreasureHunt,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDoorOpening_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "DoorOpening.h" },
		{ "ModuleRelativePath", "DoorOpening.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDoorOpening_Statics::NewProp_DoorOpenSpeed_MetaData[] = {
		{ "Category", "DoorOpening" },
		{ "ModuleRelativePath", "DoorOpening.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDoorOpening_Statics::NewProp_DoorOpenSpeed = { "DoorOpenSpeed", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDoorOpening, DoorOpenSpeed), METADATA_PARAMS(Z_Construct_UClass_UDoorOpening_Statics::NewProp_DoorOpenSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDoorOpening_Statics::NewProp_DoorOpenSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDoorOpening_Statics::NewProp_TargetDelay_MetaData[] = {
		{ "Category", "DoorOpening" },
		{ "ModuleRelativePath", "DoorOpening.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDoorOpening_Statics::NewProp_TargetDelay = { "TargetDelay", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDoorOpening, TargetDelay), METADATA_PARAMS(Z_Construct_UClass_UDoorOpening_Statics::NewProp_TargetDelay_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDoorOpening_Statics::NewProp_TargetDelay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDoorOpening_Statics::NewProp_DoorAudio_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "DoorOpening.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDoorOpening_Statics::NewProp_DoorAudio = { "DoorAudio", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDoorOpening, DoorAudio), Z_Construct_UClass_UAudioComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDoorOpening_Statics::NewProp_DoorAudio_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDoorOpening_Statics::NewProp_DoorAudio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDoorOpening_Statics::NewProp_PressurePlate1_MetaData[] = {
		{ "Category", "DoorOpening" },
		{ "Comment", "// Trigger volumes\n" },
		{ "ModuleRelativePath", "DoorOpening.h" },
		{ "ToolTip", "Trigger volumes" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDoorOpening_Statics::NewProp_PressurePlate1 = { "PressurePlate1", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDoorOpening, PressurePlate1), Z_Construct_UClass_ATriggerVolume_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDoorOpening_Statics::NewProp_PressurePlate1_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDoorOpening_Statics::NewProp_PressurePlate1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDoorOpening_Statics::NewProp_PressurePlate2_MetaData[] = {
		{ "Category", "DoorOpening" },
		{ "ModuleRelativePath", "DoorOpening.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDoorOpening_Statics::NewProp_PressurePlate2 = { "PressurePlate2", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDoorOpening, PressurePlate2), Z_Construct_UClass_ATriggerVolume_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDoorOpening_Statics::NewProp_PressurePlate2_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDoorOpening_Statics::NewProp_PressurePlate2_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDoorOpening_Statics::NewProp_PressurePlate3_MetaData[] = {
		{ "Category", "DoorOpening" },
		{ "ModuleRelativePath", "DoorOpening.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDoorOpening_Statics::NewProp_PressurePlate3 = { "PressurePlate3", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDoorOpening, PressurePlate3), Z_Construct_UClass_ATriggerVolume_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDoorOpening_Statics::NewProp_PressurePlate3_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDoorOpening_Statics::NewProp_PressurePlate3_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDoorOpening_Statics::NewProp_PressurePlate4_MetaData[] = {
		{ "Category", "DoorOpening" },
		{ "ModuleRelativePath", "DoorOpening.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDoorOpening_Statics::NewProp_PressurePlate4 = { "PressurePlate4", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDoorOpening, PressurePlate4), Z_Construct_UClass_ATriggerVolume_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDoorOpening_Statics::NewProp_PressurePlate4_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDoorOpening_Statics::NewProp_PressurePlate4_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDoorOpening_Statics::NewProp_GameWonTrigger_MetaData[] = {
		{ "Category", "DoorOpening" },
		{ "ModuleRelativePath", "DoorOpening.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDoorOpening_Statics::NewProp_GameWonTrigger = { "GameWonTrigger", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDoorOpening, GameWonTrigger), Z_Construct_UClass_ATriggerVolume_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDoorOpening_Statics::NewProp_GameWonTrigger_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDoorOpening_Statics::NewProp_GameWonTrigger_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDoorOpening_Statics::NewProp_Cube1_MetaData[] = {
		{ "Category", "DoorOpening" },
		{ "Comment", "// Trigger objects\n" },
		{ "ModuleRelativePath", "DoorOpening.h" },
		{ "ToolTip", "Trigger objects" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDoorOpening_Statics::NewProp_Cube1 = { "Cube1", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDoorOpening, Cube1), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDoorOpening_Statics::NewProp_Cube1_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDoorOpening_Statics::NewProp_Cube1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDoorOpening_Statics::NewProp_Cube2_MetaData[] = {
		{ "Category", "DoorOpening" },
		{ "ModuleRelativePath", "DoorOpening.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDoorOpening_Statics::NewProp_Cube2 = { "Cube2", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDoorOpening, Cube2), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDoorOpening_Statics::NewProp_Cube2_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDoorOpening_Statics::NewProp_Cube2_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDoorOpening_Statics::NewProp_Cube3_MetaData[] = {
		{ "Category", "DoorOpening" },
		{ "ModuleRelativePath", "DoorOpening.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDoorOpening_Statics::NewProp_Cube3 = { "Cube3", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDoorOpening, Cube3), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDoorOpening_Statics::NewProp_Cube3_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDoorOpening_Statics::NewProp_Cube3_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDoorOpening_Statics::NewProp_Cube4_MetaData[] = {
		{ "Category", "DoorOpening" },
		{ "ModuleRelativePath", "DoorOpening.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDoorOpening_Statics::NewProp_Cube4 = { "Cube4", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDoorOpening, Cube4), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDoorOpening_Statics::NewProp_Cube4_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDoorOpening_Statics::NewProp_Cube4_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDoorOpening_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDoorOpening_Statics::NewProp_DoorOpenSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDoorOpening_Statics::NewProp_TargetDelay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDoorOpening_Statics::NewProp_DoorAudio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDoorOpening_Statics::NewProp_PressurePlate1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDoorOpening_Statics::NewProp_PressurePlate2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDoorOpening_Statics::NewProp_PressurePlate3,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDoorOpening_Statics::NewProp_PressurePlate4,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDoorOpening_Statics::NewProp_GameWonTrigger,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDoorOpening_Statics::NewProp_Cube1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDoorOpening_Statics::NewProp_Cube2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDoorOpening_Statics::NewProp_Cube3,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDoorOpening_Statics::NewProp_Cube4,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDoorOpening_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDoorOpening>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDoorOpening_Statics::ClassParams = {
		&UDoorOpening::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDoorOpening_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDoorOpening_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDoorOpening_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDoorOpening_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDoorOpening()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDoorOpening_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDoorOpening, 460725288);
	template<> TREASUREHUNT_API UClass* StaticClass<UDoorOpening>()
	{
		return UDoorOpening::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDoorOpening(Z_Construct_UClass_UDoorOpening, &UDoorOpening::StaticClass, TEXT("/Script/TreasureHunt"), TEXT("UDoorOpening"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDoorOpening);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
