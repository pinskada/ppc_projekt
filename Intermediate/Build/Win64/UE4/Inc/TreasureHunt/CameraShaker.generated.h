// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TREASUREHUNT_CameraShaker_generated_h
#error "CameraShaker.generated.h already included, missing '#pragma once' in CameraShaker.h"
#endif
#define TREASUREHUNT_CameraShaker_generated_h

#define TreasureHunt_Source_TreasureHunt_Public_CameraShaker_h_14_SPARSE_DATA
#define TreasureHunt_Source_TreasureHunt_Public_CameraShaker_h_14_RPC_WRAPPERS
#define TreasureHunt_Source_TreasureHunt_Public_CameraShaker_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define TreasureHunt_Source_TreasureHunt_Public_CameraShaker_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCameraShaker(); \
	friend struct Z_Construct_UClass_UCameraShaker_Statics; \
public: \
	DECLARE_CLASS(UCameraShaker, UMatineeCameraShake, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TreasureHunt"), NO_API) \
	DECLARE_SERIALIZER(UCameraShaker)


#define TreasureHunt_Source_TreasureHunt_Public_CameraShaker_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUCameraShaker(); \
	friend struct Z_Construct_UClass_UCameraShaker_Statics; \
public: \
	DECLARE_CLASS(UCameraShaker, UMatineeCameraShake, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TreasureHunt"), NO_API) \
	DECLARE_SERIALIZER(UCameraShaker)


#define TreasureHunt_Source_TreasureHunt_Public_CameraShaker_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCameraShaker(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCameraShaker) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCameraShaker); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCameraShaker); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCameraShaker(UCameraShaker&&); \
	NO_API UCameraShaker(const UCameraShaker&); \
public:


#define TreasureHunt_Source_TreasureHunt_Public_CameraShaker_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCameraShaker(UCameraShaker&&); \
	NO_API UCameraShaker(const UCameraShaker&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCameraShaker); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCameraShaker); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCameraShaker)


#define TreasureHunt_Source_TreasureHunt_Public_CameraShaker_h_14_PRIVATE_PROPERTY_OFFSET
#define TreasureHunt_Source_TreasureHunt_Public_CameraShaker_h_11_PROLOG
#define TreasureHunt_Source_TreasureHunt_Public_CameraShaker_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TreasureHunt_Source_TreasureHunt_Public_CameraShaker_h_14_PRIVATE_PROPERTY_OFFSET \
	TreasureHunt_Source_TreasureHunt_Public_CameraShaker_h_14_SPARSE_DATA \
	TreasureHunt_Source_TreasureHunt_Public_CameraShaker_h_14_RPC_WRAPPERS \
	TreasureHunt_Source_TreasureHunt_Public_CameraShaker_h_14_INCLASS \
	TreasureHunt_Source_TreasureHunt_Public_CameraShaker_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TreasureHunt_Source_TreasureHunt_Public_CameraShaker_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TreasureHunt_Source_TreasureHunt_Public_CameraShaker_h_14_PRIVATE_PROPERTY_OFFSET \
	TreasureHunt_Source_TreasureHunt_Public_CameraShaker_h_14_SPARSE_DATA \
	TreasureHunt_Source_TreasureHunt_Public_CameraShaker_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	TreasureHunt_Source_TreasureHunt_Public_CameraShaker_h_14_INCLASS_NO_PURE_DECLS \
	TreasureHunt_Source_TreasureHunt_Public_CameraShaker_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TREASUREHUNT_API UClass* StaticClass<class UCameraShaker>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TreasureHunt_Source_TreasureHunt_Public_CameraShaker_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
