// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TREASUREHUNT_GameWonWidget_generated_h
#error "GameWonWidget.generated.h already included, missing '#pragma once' in GameWonWidget.h"
#endif
#define TREASUREHUNT_GameWonWidget_generated_h

#define TreasureHunt_Source_TreasureHunt_Public_GameWonWidget_h_13_SPARSE_DATA
#define TreasureHunt_Source_TreasureHunt_Public_GameWonWidget_h_13_RPC_WRAPPERS
#define TreasureHunt_Source_TreasureHunt_Public_GameWonWidget_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define TreasureHunt_Source_TreasureHunt_Public_GameWonWidget_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGameWonWidget(); \
	friend struct Z_Construct_UClass_UGameWonWidget_Statics; \
public: \
	DECLARE_CLASS(UGameWonWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TreasureHunt"), NO_API) \
	DECLARE_SERIALIZER(UGameWonWidget)


#define TreasureHunt_Source_TreasureHunt_Public_GameWonWidget_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUGameWonWidget(); \
	friend struct Z_Construct_UClass_UGameWonWidget_Statics; \
public: \
	DECLARE_CLASS(UGameWonWidget, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TreasureHunt"), NO_API) \
	DECLARE_SERIALIZER(UGameWonWidget)


#define TreasureHunt_Source_TreasureHunt_Public_GameWonWidget_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGameWonWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameWonWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameWonWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameWonWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameWonWidget(UGameWonWidget&&); \
	NO_API UGameWonWidget(const UGameWonWidget&); \
public:


#define TreasureHunt_Source_TreasureHunt_Public_GameWonWidget_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGameWonWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGameWonWidget(UGameWonWidget&&); \
	NO_API UGameWonWidget(const UGameWonWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGameWonWidget); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGameWonWidget); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGameWonWidget)


#define TreasureHunt_Source_TreasureHunt_Public_GameWonWidget_h_13_PRIVATE_PROPERTY_OFFSET
#define TreasureHunt_Source_TreasureHunt_Public_GameWonWidget_h_10_PROLOG
#define TreasureHunt_Source_TreasureHunt_Public_GameWonWidget_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TreasureHunt_Source_TreasureHunt_Public_GameWonWidget_h_13_PRIVATE_PROPERTY_OFFSET \
	TreasureHunt_Source_TreasureHunt_Public_GameWonWidget_h_13_SPARSE_DATA \
	TreasureHunt_Source_TreasureHunt_Public_GameWonWidget_h_13_RPC_WRAPPERS \
	TreasureHunt_Source_TreasureHunt_Public_GameWonWidget_h_13_INCLASS \
	TreasureHunt_Source_TreasureHunt_Public_GameWonWidget_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TreasureHunt_Source_TreasureHunt_Public_GameWonWidget_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TreasureHunt_Source_TreasureHunt_Public_GameWonWidget_h_13_PRIVATE_PROPERTY_OFFSET \
	TreasureHunt_Source_TreasureHunt_Public_GameWonWidget_h_13_SPARSE_DATA \
	TreasureHunt_Source_TreasureHunt_Public_GameWonWidget_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	TreasureHunt_Source_TreasureHunt_Public_GameWonWidget_h_13_INCLASS_NO_PURE_DECLS \
	TreasureHunt_Source_TreasureHunt_Public_GameWonWidget_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TREASUREHUNT_API UClass* StaticClass<class UGameWonWidget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TreasureHunt_Source_TreasureHunt_Public_GameWonWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
