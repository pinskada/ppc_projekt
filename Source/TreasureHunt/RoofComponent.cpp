// Fill out your copyright notice in the Description page of Project Settings.


#include "RoofComponent.h"
#include "Components/AudioComponent.h"

// Sets default values for this component's properties
URoofComponent::URoofComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void URoofComponent::BeginPlay()
{
	Super::BeginPlay();

	LowerRoofCheck = false;
	CheckForLowerRoofDone = true;
	UpperRoofCheck = false;
	SetElevationCheck = true;
	RAudio = false;
	GameOver = false;
	InitialElevation = GetCurrentEl().Z;
	FindRoofAudioComponent();
	FindCameraShake();
	FindMyGameModeBaseForRoofComponent();	
}

// Finds and gets CameraShake component
void URoofComponent::FindCameraShake()
{
    for (TObjectIterator<UCameraShaker> Itr; Itr; ++Itr)
    {
        if (Itr->IsA(UCameraShaker::StaticClass()))
        {
            RoofActorClass = *Itr;         
        }
    }
}

// Finds and gets GameModeBase component
void URoofComponent::FindMyGameModeBaseForRoofComponent()
{
    for (TObjectIterator<AMyGameModeBase> Itr; Itr; ++Itr)
    {
        if (Itr->IsA(AMyGameModeBase::StaticClass()))
        {
            MyGameModeBaseClass = *Itr;         
        }
    }
}

// Gets audio component and checks if present
void URoofComponent::FindRoofAudioComponent()
{
	RoofAudio = GetOwner()->FindComponentByClass<UAudioComponent>();

	if(!RoofAudio)
	{
		UE_LOG(LogTemp, Error, TEXT("No audio component found in %s!"), *GetOwner()->GetName());
	}
}

// Called every frame
void URoofComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Calls GameOver widget, when the roof is too low
	if(GetCurrentEl().Z < 175)
	{
		MyGameModeBaseClass->CallGameOverWidget();
	}
	
	// Lowers the roof when statues are in incorrect position
	if(LowerRoofCheck && CheckForLowerRoofDone)
	{
		LowerRoof(DeltaTime);
	}

	if(!LowerRoofCheck && !CheckForLowerRoofDone)
	{
		CheckForLowerRoofDone = true;
	}

	// Resets the roof to default position when game is won
	if(UpperRoofCheck)
	{
		SetDefaultRoof(DeltaTime);
	}
}

void URoofComponent::LowerRoof(float DeltaTime)
{
	// Plays audio for LowerRoof and shakes camera
	PlayRoofAudio();
	GetWorld()->GetFirstPlayerController()->PlayerCameraManager->StartCameraShake(UCameraShaker::StaticClass()); 
	
	// Sets current elevation
	CurrentElevation = GetCurrentEl().Z;
	if(SetElevationCheck)
	{
		TargetElevation = CurrentElevation - 150.f;
	}

	// Gets and sets new roof position
	NewPosition.Z = FMath::FInterpTo(CurrentElevation, TargetElevation, DeltaTime * RoofLowerSpeed, 1.f);
	GetOwner()->SetActorLocation(NewPosition, false, 0, ETeleportType::None);

	// Sets everything for next tick
	if(CurrentElevation>TargetElevation+1)
	{
		SetElevationCheck = false;
	}
	else if(CurrentElevation <= TargetElevation+5)
	{
		CheckForLowerRoofDone = false;
		SetElevationCheck = true;
		RAudio = false;
	}
}

// Moves roof to defalut position
void URoofComponent::SetDefaultRoof(float DeltaTime)
{
	CurrentElevation = GetCurrentEl().Z;

	NewPosition.Z = FMath::FInterpTo(CurrentElevation, InitialElevation, DeltaTime * RoofLowerSpeed, 1.f);
	GetOwner()->SetActorLocation(NewPosition, false, 0, ETeleportType::None);
}

// Gets current elevation
FVector URoofComponent::GetCurrentEl()
{
	FVector Position;
	Position = GetOwner()->GetActorLocation();
	return Position;
}

// Plays audio
void URoofComponent::PlayRoofAudio()
{
	if(!RoofAudio){return;}
	if(RAudio == false)
	{
		RoofAudio->Play();
		RAudio = true;
	}
}
