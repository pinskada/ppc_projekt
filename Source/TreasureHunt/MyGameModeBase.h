// Fill out your copyright notice in the Description page of Project Settings.


#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyGameModeBase.generated.h"

UCLASS()
class TREASUREHUNT_API AMyGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

protected:

	UPROPERTY(EditAnywhere, Category = "Class Types")
	TSubclassOf<UUserWidget> GameOverWidgetClass;

	UPROPERTY(VisibleInstanceOnly, Category = "Runtime")
	class UGameOverWidget* GameOverWidget;

	UPROPERTY(EditAnywhere, Category = "Class Types")
	TSubclassOf<UUserWidget> GameWonWidgetClass;

	UPROPERTY(VisibleInstanceOnly, Category = "Runtime")
	class UGameWonWidget* GameWonWidget;

public:
	void CallGameOverWidget();
	void CallGameWonWidget();
};
