// Fill out your copyright notice in the Description page of Project Settings.


#include "SpotLightActor.h"
#include "Components/SpotLightComponent.h"

// Sets default values
ASpotLightActor::ASpotLightActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	LightIntensity = 2000;
	
	SpotLight = CreateDefaultSubobject<USpotLightComponent>(TEXT("Point Light"));
	SpotLight->IsVisible();
	SpotLight->SetOuterConeAngle(3);
}

// Called when the game starts or when spawned
void ASpotLightActor::BeginPlay()
{
	Super::BeginPlay();
	LightingPressurePlateCheck();
	Visibility = true;

}

// Called every frame
void ASpotLightActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// If objects move in or out of the trigger volume, toggle light
	if(FindTriggerObject() && (Visibility == false))
	{
		SpotLight->ToggleVisibility();
		Visibility = true;
	}
	else if(!FindTriggerObject() && (Visibility == true))
	{
		SpotLight->ToggleVisibility();
		Visibility = false;
	}
}

// Checks if pressure plate is present
void ASpotLightActor::LightingPressurePlateCheck()
{
	if(!LightingPressurePlate)
	{
		UE_LOG(LogTemp, Error, TEXT("%s has the OpenDoor component on it, but no PressurePlate set!"), *GetOwner()->GetName());
	}
}

// Check if object is in trigger volume
bool ASpotLightActor::FindTriggerObject()
{
	if(LightingPressurePlate && LightingPressurePlate->IsOverlappingActor(TriggerObject))
	{
		return true;
	}
	else
	{
		return false;
	}
	return false;
}
