// Fill out your copyright notice in the Description page of Project Settings.


#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CameraShaker.h"
#include "MyGameModeBase.h"
#include "GameFramework/Actor.h"
#include "RoofComponent.generated.h"

class UCameraShaker;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TREASUREHUNT_API URoofComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	URoofComponent();
	void LowerRoof(float DeltaTime);
	void SetDefaultRoof(float DeltaTime);
	void PlayRoofAudio();
	void FindRoofAudioComponent();
	void FindCameraShake();
	void FindMyGameModeBaseForRoofComponent();
	FVector GetCurrentEl();
	UCameraShaker* RoofActorClass;
	AMyGameModeBase* MyGameModeBaseClass;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<UCameraShaker> CamShake;

	UPROPERTY(EditAnywhere, Category = "Class Types")
	TSubclassOf<UUserWidget> WidgetClass;

	UPROPERTY(VisibleInstanceOnly, Category = "Runtime")
	class UGameOverWidget* GameOverWidget;
	

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	bool LowerRoofCheck;
	bool CheckForLowerRoofDone;
	bool UpperRoofCheck;
	bool SetElevationCheck;
	float InitialElevation;
	float CurrentElevation;
	float TargetElevation;
	FVector NewPosition;
	bool RAudio;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	bool GameOver;

	UPROPERTY(EditAnywhere)
	float RoofLowerSpeed = 1.f;

	UPROPERTY()
	UAudioComponent* RoofAudio = nullptr;
};
