// Fill out your copyright notice in the Description page of Project Settings.


#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "MyGameModeBase.h"
#include "Engine/TriggerVolume.h"
#include "GrabbComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TREASUREHUNT_API UGrabbComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabbComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	// Trigger volumes
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class Types")
	ATriggerVolume* GameWonTrigger;

private:

	UPhysicsHandleComponent* PhysicsHandle = nullptr;
	UInputComponent* InputComponent = nullptr;

	void FindPhysicsHandle();
	void SetupInputComponent();
	void Grab();
	void Release();
	
	FVector FindLineTraceStart() const;
	FVector FindLineTraceEnd() const;
	FHitResult GetFirstPhysicsBodyInReach() const;

	UPROPERTY(EditAnywhere)
	float Reach = 125.f;	
};
