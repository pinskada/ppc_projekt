// Fill out your copyright notice in the Description page of Project Settings.


#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/TriggerVolume.h"
#include "SpotLightActor.generated.h"

class USpotLightComponent;

UCLASS()
class TREASUREHUNT_API ASpotLightActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpotLightActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, Category = "Light Switch")
	class USpotLightComponent* SpotLight;

	UPROPERTY(VisibleAnywhere, Category = "Light Switch")
	float LightIntensity;

	UPROPERTY(EditAnywhere, Category = "Light Switch")
	ATriggerVolume* LightingPressurePlate;

	UPROPERTY(EditAnywhere, Category = "Light Switch")
	AActor* TriggerObject;

	void LightingPressurePlateCheck();

	bool FindTriggerObject();

	bool Visibility;
};
