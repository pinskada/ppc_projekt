// Fill out your copyright notice in the Description page of Project Settings.


#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraShake.h"
#include "CameraShaker.generated.h"


UCLASS()
class TREASUREHUNT_API UCameraShaker : public UMatineeCameraShake
{
	GENERATED_BODY()

public:

	UCameraShaker();
	
};
