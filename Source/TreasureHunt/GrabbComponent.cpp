// Fill out your copyright notice in the Description page of Project Settings.


#include "GrabbComponent.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "DrawDebugHelpers.h"
#define OUT

// Sets default values for this component's properties
UGrabbComponent::UGrabbComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UGrabbComponent::BeginPlay()
{
	Super::BeginPlay();

	FindPhysicsHandle();
	SetupInputComponent();
}

// Gets PhysicsHandle and checks if present
void UGrabbComponent::FindPhysicsHandle()
{
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (PhysicsHandle == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("No physics handle component found on %s!"), *GetOwner()->GetName());
	}
}

// Sets all input
void UGrabbComponent::SetupInputComponent()
{
	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();

	if (InputComponent)
	{
	
		InputComponent->BindAction("Grab", IE_Pressed, this, &UGrabbComponent::Grab);
		InputComponent->BindAction("Grab", IE_Released, this, &UGrabbComponent::Release);
	}	
}

// Called every frame
void UGrabbComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Draws a white point in the middle of screen
	DrawDebugPoint(GetWorld(), FindLineTraceEnd(), 5.f, FColor::White, false, 0.016f, 0);

	// Checks if anthing is held and sets it new loaction (moves it)
	if (PhysicsHandle->GrabbedComponent)
	{
		PhysicsHandle->SetTargetLocation(FindLineTraceEnd());
	}
}

// Grabs objects
void UGrabbComponent::Grab()
{
	//Sets object in reach
	FHitResult HitResult = GetFirstPhysicsBodyInReach();
	UPrimitiveComponent* ComponentToGrab = HitResult.GetComponent();

	if (HitResult.GetActor())
	{
		PhysicsHandle->GrabComponentAtLocation
		(
			ComponentToGrab,
			NAME_None,
			FindLineTraceEnd()
		);
	}		
}

// Releases object
void UGrabbComponent::Release()
{
	PhysicsHandle->ReleaseComponent();
}

// Finds and sets objects in reach to Hit
FHitResult UGrabbComponent::GetFirstPhysicsBodyInReach() const
{
	FHitResult Hit;

	// Finds object
	FCollisionQueryParams TraceParams(FName(TEXT("")), false, GetOwner());
	GetWorld()->LineTraceSingleByObjectType(
		OUT Hit,
		FindLineTraceStart(),
		FindLineTraceEnd(),
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		TraceParams
	);

	// Sets object
	AActor* ActorHit = Hit.GetActor();

	return Hit;
}

// Returns player location
FVector UGrabbComponent::FindLineTraceStart() const
{
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPointLocation,
		OUT PlayerViewPointRotation
	);

	return PlayerViewPointLocation;
}

// Returns line end location
FVector UGrabbComponent::FindLineTraceEnd() const
{
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPointLocation,
		OUT PlayerViewPointRotation
	);
	
	return PlayerViewPointLocation + PlayerViewPointRotation.Vector() * Reach;
}
