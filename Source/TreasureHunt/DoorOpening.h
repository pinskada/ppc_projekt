// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/TriggerVolume.h"
#include "RoofComponent.h"
#include "MyGameModeBase.h"
#include "DoorOpening.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TREASUREHUNT_API UDoorOpening : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UDoorOpening();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void OpenDoor(float);
	void GetElevation();
	void FindDoorAudioComponent();
	void PlayDoorAudio();
	void PressurePlateCheck();
	bool DoorOpenCheck();
	void FindRoofComponent();
	void CheckIfWin();
	void FindMyGameModeBaseForDoorOpening();
	URoofComponent* DoorActorClass;
	AMyGameModeBase* MyGameModeBaseClassForDoorOpening;

private:
	
	float InitialEl = 0.f;
	float TargetEl = 0.f;
	float CurrentEl = 0.f;
	float SoundDelay = 0.f;
	
	bool DAudio;

	AActor* TriggerActor;

	UPROPERTY(EditAnywhere)
	float DoorOpenSpeed = 1.f;

	UPROPERTY(EditAnywhere)
	float TargetDelay = 2.f;

	UPROPERTY()
	UAudioComponent* DoorAudio = nullptr;

	// Trigger volumes
	UPROPERTY(EditAnywhere)
	ATriggerVolume* PressurePlate1;
	UPROPERTY(EditAnywhere)
	ATriggerVolume* PressurePlate2;
	UPROPERTY(EditAnywhere)
	ATriggerVolume* PressurePlate3;
	UPROPERTY(EditAnywhere)
	ATriggerVolume* PressurePlate4;

	UPROPERTY(EditAnywhere)
	ATriggerVolume* GameWonTrigger;

	// Trigger objects
	UPROPERTY(EditAnywhere)
	AActor* Cube1;
	UPROPERTY(EditAnywhere)
	AActor* Cube2;
	UPROPERTY(EditAnywhere)
	AActor* Cube3;
	UPROPERTY(EditAnywhere)
	AActor* Cube4;
};
