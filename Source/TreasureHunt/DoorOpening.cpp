// Fill out your copyright notice in the Description page of Project Settings.


#include "DoorOpening.h"
#include "Components/AudioComponent.h"
#include "Components/PrimitiveComponent.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h" 
#include "GameFramework/Actor.h"

#define OUT


// Sets default values for this component's properties
UDoorOpening::UDoorOpening()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UDoorOpening::BeginPlay()
{
	Super::BeginPlay();

	GetElevation();
	FindDoorAudioComponent();
	PressurePlateCheck();
	FindRoofComponent();
	FindMyGameModeBaseForDoorOpening();
	DAudio = false;

	TriggerActor = GetWorld()->GetFirstPlayerController()->GetPawn();

}

// Finds and gets RoofComponent
void UDoorOpening::FindRoofComponent()
{
    for (TObjectIterator<URoofComponent> Itr; Itr; ++Itr)
    {
        if (Itr->IsA(URoofComponent::StaticClass()))
        {
            DoorActorClass = *Itr;         
        }
    }
}

// Finds and gets GameModeBase component
void UDoorOpening::FindMyGameModeBaseForDoorOpening()
{
    for (TObjectIterator<AMyGameModeBase> Itr; Itr; ++Itr)
    {
        if (Itr->IsA(AMyGameModeBase::StaticClass()))
        {
            MyGameModeBaseClassForDoorOpening = *Itr;         
        }
    }
}

// Gets door initial elevation and sets target elevation
void UDoorOpening::GetElevation()
{
	InitialEl = GetOwner()->GetActorLocation().Z;
	TargetEl = InitialEl + 215;
}

// Gets audio component and checks if present
void UDoorOpening::FindDoorAudioComponent()
{
	DoorAudio = GetOwner()->FindComponentByClass<UAudioComponent>();

	if(!DoorAudio)
	{
		UE_LOG(LogTemp, Error, TEXT("No audio component found in %s!"), *GetOwner()->GetName());
	}
}

// Checks if pressure plate is assigned
void UDoorOpening::PressurePlateCheck()
{
	if(!PressurePlate1 || !PressurePlate2 || !PressurePlate3 || !PressurePlate4 || !GameWonTrigger)
	{
		UE_LOG(LogTemp, Error, TEXT("%s has the OpenDoor component on it, but no PressurePlate set!"), *GetOwner()->GetName());
	}
}

// Called every frame
void UDoorOpening::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if(DoorOpenCheck())
	{
		OpenDoor(DeltaTime);
	}
	
	CheckIfWin();
}

// Checks for all conditions to open the door
bool UDoorOpening::DoorOpenCheck()
{
	bool Result = false;
	
	// PressurePlate has to be present and all assigned objects
	if
	(
		PressurePlate1 && 
		PressurePlate2 &&
		PressurePlate3 &&
		PressurePlate4 &&
		PressurePlate1->IsOverlappingActor(Cube1) &&
		PressurePlate2->IsOverlappingActor(Cube2) &&
		PressurePlate3->IsOverlappingActor(Cube3) &&
		PressurePlate4->IsOverlappingActor(Cube4)		
	)
	{
		Result = true;
		DoorActorClass->UpperRoofCheck = true;
	}
	
	// If all PressurePlates are activated but in wrong order, lower the roof
	else if
	(
		PressurePlate1 && 
		PressurePlate2 &&
		PressurePlate3 &&
		PressurePlate4 &&
		(	
			PressurePlate1->IsOverlappingActor(Cube1) ||
			PressurePlate1->IsOverlappingActor(Cube2) ||
			PressurePlate1->IsOverlappingActor(Cube3) ||
			PressurePlate1->IsOverlappingActor(Cube4)
		)
		&&
		(
			PressurePlate2->IsOverlappingActor(Cube1) ||
			PressurePlate2->IsOverlappingActor(Cube2) ||
			PressurePlate2->IsOverlappingActor(Cube3) ||
			PressurePlate2->IsOverlappingActor(Cube4) 
		)
		&&
		(
			PressurePlate3->IsOverlappingActor(Cube1) ||
			PressurePlate3->IsOverlappingActor(Cube2) ||
			PressurePlate3->IsOverlappingActor(Cube3) ||
			PressurePlate3->IsOverlappingActor(Cube4)

		)
		&&
		(
			PressurePlate4->IsOverlappingActor(Cube1) ||
			PressurePlate4->IsOverlappingActor(Cube2) ||	
			PressurePlate4->IsOverlappingActor(Cube3) ||
			PressurePlate4->IsOverlappingActor(Cube4) 	
		)
	)
	{
		DoorActorClass->LowerRoofCheck = true;
	}
	else
	{
		DoorActorClass->LowerRoofCheck = false;
	}

	return Result;
}

// Opens door and plays audio
void UDoorOpening::OpenDoor(float DeltaTime)
{
	// Plays audio
	PlayDoorAudio();
	
	// Sets delay for opening the door,
	// because the audio tape plays nothing the first second
	SoundDelay = FMath::FInterpTo(SoundDelay, 5, DeltaTime, 1.f);
	
	// Checks if the delay has passed and opens the door
	if(SoundDelay > TargetDelay)
	{
		// Checks for current elevation every frame and gradually 
		// opens the door by setting the new value every frame
		CurrentEl = GetOwner()->GetActorLocation().Z;
		FVector OpenLocation = GetOwner()->GetActorLocation();

		OpenLocation.Z = FMath::FInterpTo(CurrentEl, TargetEl, DeltaTime * DoorOpenSpeed, 1.f);

		GetOwner()->SetActorLocation(OpenLocation, false, 0, ETeleportType::None);
	}
}

// Plays audio
void UDoorOpening::PlayDoorAudio()
{
	if(!DoorAudio){return;}
	if(DAudio == false)
	{
		DoorAudio->Play();
		DAudio = true;
	}
}

// Checks if trriger for GameWin widget is active and calls widget
void UDoorOpening::CheckIfWin()
{
	if(GameWonTrigger && GameWonTrigger->IsOverlappingActor(TriggerActor))
	{
		MyGameModeBaseClassForDoorOpening->CallGameWonWidget();
	}
}
