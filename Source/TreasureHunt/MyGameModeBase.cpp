// Fill out your copyright notice in the Description page of Project Settings.


#include "MyGameModeBase.h"
#include "GameOverWidget.h"
#include "GameWonWidget.h"
#include "Blueprint/UserWidget.h"

// Shows GameOver widget
void AMyGameModeBase::CallGameOverWidget()
{
    if(IsValid(GameOverWidgetClass))
    {
        GameOverWidget = Cast<UGameOverWidget>(CreateWidget(GetWorld(), GameOverWidgetClass));

        if(GameOverWidget)
        {
            GameOverWidget->AddToViewport();
        }
    }
}

// Shows GameWon widget
void AMyGameModeBase::CallGameWonWidget()
{
    if(IsValid(GameWonWidgetClass))
    {
        GameWonWidget = Cast<UGameWonWidget>(CreateWidget(GetWorld(), GameWonWidgetClass));

        if(GameWonWidget)
        {
            GameWonWidget->AddToViewport();
        }
    }
}
